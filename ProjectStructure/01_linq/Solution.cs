﻿using System;
using System.Collections.Generic;
using System.Text;
using Models.Models;
using System.Linq;

namespace _01_linq
{
    static class Solution
    {
       public static void task_01(List<Project> data, int authorId)
        {
            var result = data
                            .Where(p => p.Author.Id == authorId)
                            .ToDictionary(p => p,
                                          p => p.Tasks.Count());

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task01");
            Console.WriteLine(new string('-', 20));
            foreach (var pair in result)
            {
                Console.WriteLine("\t" + new string('*', 20));
                Console.WriteLine("\t" + "ProjectId = " + pair.Key.Id);
                Console.WriteLine("\t" + "Task count = " + pair.Value);
                Console.WriteLine("\t" + new string('*', 20));
            }
        }

        public static void task_02(List<Project> data, int userId)
        {
            var result = data
                            .SelectMany(p => p.Tasks)
                            .Where(t => t.Performer.Id == userId && t.Name.Length < 45);


            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task02");
            Console.WriteLine(new string('-', 20));
            foreach (var t in result)
            {
                Console.WriteLine("\t" + new string('*', 20));
                Console.WriteLine("\t" + "TaskId = " + t.Id);
                Console.WriteLine("\t" + "Name = " + t.Name);
                Console.WriteLine("\t" + new string('*', 20));
            }
        }

        public static void task_03(List<Project> data, int userId)
        {
            var result = data
                            .SelectMany(p => p.Tasks)
                            .Where(t => t.Performer.Id == userId
                                     && t.FinishedAt?.Year == DateTime.Now.Year)
                            .Select(t => new { t.Id, t.Name });

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task03");
            Console.WriteLine(new string('-', 20));
            foreach (var t in result)
            {
                Console.WriteLine("\t" + new string('*', 20));
                Console.WriteLine("\t" + "TaskId = " + t.Id);
                Console.WriteLine("\t" + "Name = " + t.Name);
                Console.WriteLine("\t" + new string('*', 20));
            }
        }

        public static void task_04(List<Project> data)
        {
            var result = data
                         .Select(p => p.Team).Distinct()
                         .GroupJoin(
                                     data
                                     .Select(p => p.Author)
                                     .Union(
                                         data
                                         .SelectMany(p => p.Tasks)
                                         .Select(t => t.Performer))
                                     .Distinct(),
                                     t => t.Id,
                                     u => u.TeamId,
                                     (t, u) => new { Team = t, Users = u.ToList() }
                                     )
                         .Where(t => t.Users.All(user => DateTime.Now.Year - user.BirthDay.Year > 10))
                         .Select(t => new
                         {
                             id = t.Team.Id,
                             name = t.Team.Name,
                             users = t.Users.OrderByDescending(user => user.RegisteredAt)
                         }
                         );

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task04");
            Console.WriteLine(new string('-', 20));
            foreach (var t in result)
            {
                Console.WriteLine(new string('*', 20));
                Console.WriteLine("TeamId = " + t.id);
                Console.WriteLine("Name = " + t.name);
                foreach (var u in t.users)
                {
                    Console.WriteLine("\tUserId = " + u.Id);
                    Console.WriteLine("\tName = " + u.FirstName);
                    Console.WriteLine("\tBirthDay = " + u.BirthDay);
                    Console.WriteLine("\tRegisterAt = " + u.RegisteredAt);
                    Console.WriteLine("\t-----");

                }
                Console.WriteLine(new string('*', 20));
            }
        }

        public static void task_05(List<Project> data)
        {
            var result = data
                         .Select(p => p.Author)
                         .Union(data
                                .SelectMany(p => p.Tasks)
                                .Select(t => t.Performer))
                         .Distinct()
                         .GroupJoin(
                            data.SelectMany(p => p.Tasks),
                            user => user.Id,
                            task => task.Performer.Id,
                            (user, tasks) => new {user, tasks = tasks.ToList().OrderByDescending(t => t.Name.Length)}
                          )
                         .OrderBy(u => u.user.FirstName);

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task05");
            Console.WriteLine(new string('-', 20));
            foreach (var u in result)
            {
                Console.WriteLine(new string('*', 20));
                Console.WriteLine("UserId = " + u.user.Id);
                Console.WriteLine("Name = " + u.user.FirstName);
                foreach (var t in u.tasks)
                {
                    Console.WriteLine("\tTaskId = " + t.Id);
                    Console.WriteLine("\tName = " + t.Name);
                    Console.WriteLine("\t-----");

                }
                Console.WriteLine(new string('*', 20));
            }
        }

        public static void task_06(List<Project> data, int userId)
        {
            var result = data
                         .Select(p => p.Author)
                         .Union(data
                                .SelectMany(p => p.Tasks)
                                .Select(t => t.Performer))
                         .Distinct()
                         .Where(u => u.Id == userId)
                         .GroupJoin(
                            data,
                            user => user.Id,
                            project => project.Author.Id,
                            (user, projects) => new { user, projects = projects.OrderByDescending(p => p.CreatedAt) }
                         )
                         .GroupJoin(
                            data.SelectMany(p => p.Tasks),
                            userInfo => userInfo.user.Id,
                            task => task.Performer.Id,
                            (userInfo, tasks) => new { userInfo.user, 
                                                       userInfo.projects, 
                                                       tasks = tasks.OrderByDescending(t => t.FinishedAt.HasValue ? (t.FinishedAt.Value - t.CreatedAt).Ticks : -1) }
                         ).
                         Select(info =>
                         {
                             var lastProject = info.projects.FirstOrDefault();
                             var lastProjectTasksCount = lastProject != null ? lastProject.Tasks.ToList().Count : 0;
                             return new
                             {
                                 info.user,
                                 lastProject,
                                 lastProjectTasksCount,
                                 unfinishedTasksCount = info.tasks.Where(t => (!t.FinishedAt.HasValue || t.State != 2)).ToList().Count,
                                 longestTask = info.tasks.FirstOrDefault()
                             };
                         });

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task06");
            Console.WriteLine(new string('-', 20));
            foreach (var info in result)
            {
                Console.WriteLine("\t" + new string('*', 20));
                Console.WriteLine("\tUserId = " + info.user.Id);

                if(info.lastProject != null)
                {
                    Console.WriteLine("\tlastProjectId = " + info.lastProject.Id);
                    Console.WriteLine("\tlastProjectTasksAmount = " + info.lastProjectTasksCount);
                }
                Console.WriteLine("\tunfinishedTasksAmount = " + info.unfinishedTasksCount);

                if(info.longestTask != null)
                {
                    Console.WriteLine("\tlongestTask.createdAt = " + info.longestTask.CreatedAt);
                    Console.WriteLine("\tlongestTask.finishedAt = " + info.longestTask.FinishedAt);
                }
                Console.WriteLine("\t" + new string('*', 20));
            }
        }

    public static void task_07(List<Project> data)
        {
            var result = data
                         .GroupJoin(
                            data.Select(p => p.Author)
                                .Union(data
                                        .SelectMany(p => p.Tasks)
                                        .Select(t => t.Performer)
                                .Distinct()),
                            project => project.Team.Id,
                            user => user.TeamId,
                            (project, users) => new { project, Team = users }
                          )
                         .Select(info =>
                         {
                             var usersInTeamCount = (info.project.Description.Length > 20 
                                                  || info.project.Tasks.ToList().Count < 3)
                                                    ?
                                                    (Nullable<int>) info.Team.ToList().Count
                                                    :
                                                    null;
                             
                                return
                                new
                                {
                                    info.project,
                                    longestTask = info.project.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                                    shortestTask = info.project.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                                    usersInTeamCount
                                };
                         });

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task07");
            Console.WriteLine(new string('-', 20));
            foreach (var info in result)
            {
                Console.WriteLine(new string('*', 20));
                Console.WriteLine("projectId = " + info.project.Id);
                Console.WriteLine("longestTask = " + (info.longestTask != null ? info.longestTask.Id.ToString() : "null"));
                Console.WriteLine("shortestTask = " + (info.shortestTask != null ? info.shortestTask.Id.ToString() : "null"));
                Console.WriteLine("usersInTeamCount = " + (info.usersInTeamCount != null ? info.usersInTeamCount.ToString() : "null"));
                Console.WriteLine(new string('*', 20));
            }
        }
    }
}
