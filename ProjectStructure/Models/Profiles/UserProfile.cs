﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Models.DTO;
using Models.Models;

namespace Models.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();

            CreateMap<UserDTO, User>();
        }
    }
}
