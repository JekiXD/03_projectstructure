﻿using Models.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Models.Models
{
    public class User : BaseEntity
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get { return CreatedAt; } set { CreatedAt = value; } }
        public DateTime BirthDay { get; set; }
    }
}
