﻿using Models.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Models
{
    public class Task : BaseEntity
    {
        public int ProjectId { get; set; }
        public User Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
