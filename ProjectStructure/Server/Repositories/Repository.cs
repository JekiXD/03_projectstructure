﻿using Models.Abstract;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Server.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected List<TEntity> data;

        public Repository()
        {
            data = new List<TEntity>();
        }
        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            if (filter != null) return data.Where(filter.Compile());

            return data;
        }
        public virtual void Create(TEntity entity, string createdBy = null)
        {
            var result = data.Find(e => e.Id == entity.Id);
            if (result != null) throw new ArgumentException("Entity with such id already exist");
            data.Add(entity);
        }
        public virtual void Update(TEntity entity, string modifiedBy = null)
        {
            data = data.Select(e => e.Id == entity.Id ? entity : e).ToList();
        }
        public virtual void Delete(object id)
        {
            var entity = data.Find(e => e.Id == (int)id);
            Delete(entity);
        }
        public virtual void Delete(TEntity entity)
        {
            data.Remove(entity);
        }
    }
}
