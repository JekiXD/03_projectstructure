﻿using AutoMapper;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MM = Models.Models;
using Models.DTO;

namespace Server.Services
{
    public class TeamService
    {
        IRepository<MM.Team> _teamRepository;
        IMapper _mapper;
        public TeamService(IRepository<MM.Team> teamRepository, IMapper mapper)
        {
            _teamRepository = teamRepository;
            _mapper = mapper;
        }

        public IEnumerable<TeamDTO> GetTeams()
        {
            var teamSet = _teamRepository.Get();

            return _mapper.Map<IEnumerable<TeamDTO>>(teamSet);
        }

        public TeamDTO GetTeamById(int id)
        {
            var teamSet = _teamRepository.Get().Where(t => t.Id == id);

            if (teamSet.Count() == 0) return null;

            return _mapper.Map<TeamDTO>(teamSet.First());
        }

        public void AddTeam(TeamDTO team)
        {
            MM.Team newTeam = _mapper.Map<MM.Team>(team);
            _teamRepository.Create(newTeam);
        }

        public void UpdateTeam(TeamDTO team)
        {
            MM.Team newteam = _mapper.Map<MM.Team>(team);
            _teamRepository.Update(newteam);
        }

        public void DeleteTeam(int id)
        {
            _teamRepository.Delete(id);
        }
    }
}
