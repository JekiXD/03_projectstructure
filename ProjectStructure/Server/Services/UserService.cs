﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models.Models;
using Server.Repositories;
using Server.Interfaces;
using Models.DTO;
using AutoMapper;

namespace Server.Services
{
    public class UserService
    {
        IRepository<User> _repository;
        IMapper _mapper;
        public UserService(IRepository<User> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            var userSet = _repository.Get();

            return _mapper.Map<IEnumerable<UserDTO>>(userSet);
        }

        public UserDTO GetUserById(int id)
        {
            var userSet = _repository.Get().Where(u => u.Id == id);

            if (userSet.Count() == 0) return null;

            return _mapper.Map<UserDTO>(userSet.First());
        }

        public void AddUser(UserDTO user)
        {
            User newUser = _mapper.Map<User>(user);
            _repository.Create(newUser);
        }

        public void UpdateUser(UserDTO user)
        {
            User newUser = _mapper.Map<User>(user);
            _repository.Update(newUser);
        }

        public void DeleteUser(int id)
        {
            _repository.Delete(id);
        }
    }
}
