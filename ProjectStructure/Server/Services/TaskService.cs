﻿using AutoMapper;
using MM = Models.Models;
using Models.DTO;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TaskService
    {
        IRepository<MM.Task> _taskRepository;
        IRepository<MM.User> _userRepository;
        IMapper _mapper;
        public TaskService(IRepository<MM.Task> taskRepository, IRepository<MM.User> userRepository, IMapper mapper)
        {
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public IEnumerable<TaskDTO> GetTasks()
        {
            var taskSet = _taskRepository.Get();

            return _mapper.Map<IEnumerable<TaskDTO>>(taskSet);
        }

        public TaskDTO GetTaskById(int id)
        {
            var taskSet = _taskRepository.Get().Where(t => t.Id == id);

            if (taskSet.Count() == 0) return null;

            return _mapper.Map<TaskDTO>(taskSet.First());
        }

        public void AddTask(TaskDTO task)
        {
            MM.Task newTask = _mapper.Map<MM.Task>(task);
            MM.User taskPerformer = getTaskPerformer(task);

            newTask.Performer = taskPerformer;
            _taskRepository.Create(newTask);
        }

        public void UpdateTask(TaskDTO task)
        {
            MM.Task newTask = _mapper.Map<MM.Task>(task);
            MM.User taskPerformer = getTaskPerformer(task);

            newTask.Performer = taskPerformer;
            _taskRepository.Update(newTask);
        }

        public void DeleteTask(int id)
        {
            _taskRepository.Delete(id);
        }

        MM.User getTaskPerformer(TaskDTO task)
        {
            var userSet = _userRepository.Get().Where(u => u.Id == task.PerformerId);

            if (userSet.Count() != 0) return userSet.First();
            return null;
        }
    }
}
