﻿using AutoMapper;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MM = Models.Models;
using Models.DTO;

namespace Server.Services
{
    public class ProjectService
    {
        IRepository<MM.Project> _projectRepository;
        IRepository<MM.User> _userRepository;
        IRepository<MM.Team> _teamRepository;
        IRepository<MM.Task> _taskRepository;
        IMapper _mapper;
        public ProjectService(IRepository<MM.Project> projectRepository,
                              IRepository<MM.User> userRepository,
                              IRepository<MM.Team> teamRepository,
                              IRepository<MM.Task> taskRepository,
                              IMapper mapper)
        {
            _projectRepository = projectRepository;
            _userRepository = userRepository;
            _teamRepository = teamRepository;
            _taskRepository = taskRepository;
            _mapper = mapper;
        }

        public IEnumerable<ProjectDTO> GetProjects()
        {
            var projectSet = _projectRepository.Get();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projectSet);
        }

        public ProjectDTO GetProjectById(int id)
        {
            var projectSet = _projectRepository.Get().Where(p => p.Id == id);

            if (projectSet.Count() == 0) return null;

            return _mapper.Map<ProjectDTO>(projectSet.First());
        }

        public void AddProject(ProjectDTO project)
        {
            var newProject = _mapper.Map<MM.Project>(project);
            var author = getProjectAuthor(project);
            var team = getProjectTeam(project);
            var tasks = getProjectTasks(project);

            newProject.Author = author;
            newProject.Team = team;
            newProject.Tasks = tasks;
            _projectRepository.Create(newProject);
        }

        public void UpdateProject(ProjectDTO project)
        {
            var newProject = _mapper.Map<MM.Project>(project);
            var author = getProjectAuthor(project);
            var team = getProjectTeam(project);
            var tasks = getProjectTasks(project);

            newProject.Author = author;
            newProject.Team = team;
            newProject.Tasks = tasks;
            _projectRepository.Update(newProject);
        }

        public void DeleteProject(int id)
        {
            _projectRepository.Delete(id);
        }

        public MM.User getProjectAuthor(ProjectDTO project)
        {
            var user = _userRepository.Get().Where(u => u.Id == project.AuthorId);

            if (user.Count() == 0) return null;
            return user.First();
        }

        public MM.Team getProjectTeam(ProjectDTO project)
        {
            var team = _teamRepository.Get().Where(t => t.Id == project.TeamId);

            if (team.Count() == 0) return null;
            return team.First();
        }

        public ICollection<MM.Task> getProjectTasks(ProjectDTO project)
        {
            var tasks = _taskRepository.Get().Where(t => t.ProjectId == project.Id);
            return tasks.ToList();
        }
    }
}
