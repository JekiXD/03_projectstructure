﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        ProjectService _service;
        public ProjectController(ProjectService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public ActionResult<IEnumerable<ProjectDTO>> Read()
        {
            return Ok(_service.GetProjects());
        }

        [HttpGet]
        [Route("read/{id}")]
        public ActionResult<ProjectDTO> Read(int id)
        {
            ProjectDTO project = _service.GetProjectById(id);

            if (project == null) return NotFound();
            return Ok(project);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody] ProjectDTO project)
        {
            _service.AddProject(project);
            return NoContent();
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update([FromBody] ProjectDTO project)
        {
            _service.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult Delete(int id)
        {
            _service.DeleteProject(id);
            return NoContent();
        }
    }
}
