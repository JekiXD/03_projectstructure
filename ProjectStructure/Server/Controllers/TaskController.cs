﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        TaskService _service;
        public TaskController(TaskService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public ActionResult<IEnumerable<TaskDTO>> Read()
        {
            return Ok(_service.GetTasks());
        }

        [HttpGet]
        [Route("read/{id}")]
        public ActionResult<TaskDTO> Read(int id)
        {
            TaskDTO task = _service.GetTaskById(id);

            if (task == null) return NotFound();
            return Ok(task);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody] TaskDTO task)
        {
            _service.AddTask(task);
            return NoContent();
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update([FromBody] TaskDTO task)
        {
            _service.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult Delete(int id)
        {
            _service.DeleteTask(id);
            return NoContent();
        }
    }
}
