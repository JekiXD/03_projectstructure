﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        TeamService _service;
        public TeamController(TeamService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public ActionResult<IEnumerable<TeamDTO>> Read()
        {
            return Ok(_service.GetTeams());
        }

        [HttpGet]
        [Route("read/{id}")]
        public ActionResult<TeamDTO> Read(int id)
        {
            TeamDTO team = _service.GetTeamById(id);

            if (team == null) return NotFound();
            return Ok(team);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody] TeamDTO team)
        {
            _service.AddTeam(team);
            return NoContent();
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update([FromBody] TeamDTO team)
        {
            _service.UpdateTeam(team);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult Delete(int id)
        {
            _service.DeleteTeam(id);
            return NoContent();
        }
    }
}
