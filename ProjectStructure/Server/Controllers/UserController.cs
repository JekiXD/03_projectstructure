﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        UserService _service;
        public UserController(UserService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public ActionResult<IEnumerable<UserDTO>> Read()
        {
            return Ok(_service.GetUsers());
        }

        [HttpGet]
        [Route("read/{id}")]
        public ActionResult<UserDTO> Read(int id)
        {
            UserDTO user = _service.GetUserById(id);

            if (user == null) return NotFound();
            return Ok(user);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody] UserDTO user)
        {
            _service.AddUser(user);
            return NoContent();
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update([FromBody] UserDTO user)
        {
            _service.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult Delete(int id)
        {
            _service.DeleteUser(id);
            return NoContent();
        }
    }
}
