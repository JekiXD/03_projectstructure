﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Models.Abstract;

namespace Server.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null);
        void Create(TEntity entity, string createdBy = null);
        void Update(TEntity entity, string modifiedBy = null);
        void Delete(object id);
        void Delete(TEntity entity);
    }
}
